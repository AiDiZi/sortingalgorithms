package search;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTest {

    private final BinarySearch _binarySearch = new BinarySearch();

    @Test
    public void TestFindIndex_SORTED_ARR_EXIST_ELEMENT(){
        var index = _binarySearch.findIndex(new Integer[] {0, 10, 15, 100}, 15);
        Assert.assertEquals(index, 2);
    }

    @Test
    public void TestFindIndex_SORTED_ARR_NOT_EXIST_ELEMENT(){
        var index = _binarySearch.findIndex(new Integer[] {0, 2, 90, 100}, 5);
        Assert.assertEquals(index, -1);
    }

}