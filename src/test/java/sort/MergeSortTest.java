package sort;

import org.junit.Assert;
import org.junit.Test;

public class MergeSortTest {

    MergeSort _mergeSort = new MergeSort();

    @Test
    public void TestDoSort_EMPTY_ARR(){
        var emptyArr = new Integer[] {};
        Assert.assertArrayEquals(_mergeSort.doSort(emptyArr), new Integer[] {});
    }

    @Test
    public void TestDoSort_ONE_LENGHT_ARR(){
        var oneLenghtArr = new Integer[] {1};
        Assert.assertArrayEquals(_mergeSort.doSort(oneLenghtArr), new Integer[] {1});
    }

    @Test
    public void TestDoSort_TWO_LENGHT_ARR(){
        var oneLenghtArr = new Integer[] {20,10};
        Assert.assertArrayEquals(_mergeSort.doSort(oneLenghtArr), new Integer[] {10, 20});
    }

    @Test
    public void TestDoSort_INTEGER_ARR(){
        var oneLenghtArr = new Integer[] {20,100,10,5000};
        Assert.assertArrayEquals(_mergeSort.doSort(oneLenghtArr), new Integer[] {10, 20, 100, 5000});
    }

    @Test
    public void TestDoSort_DOUBLE_ARR(){
        var oneLenghtArr = new Double[] {1.0, 100.0, 30.0};
        Assert.assertArrayEquals(_mergeSort.doSort(oneLenghtArr), new Double[] {1.0, 30.0, 100.0});
    }

    @Test
    public void TestDoSort_STRING_ARR(){
        var oneLenghtArr = new String[] {"c", "b", "a"};
        Assert.assertArrayEquals(_mergeSort.doSort(oneLenghtArr), new String[] {"a", "b", "c"});
    }


}