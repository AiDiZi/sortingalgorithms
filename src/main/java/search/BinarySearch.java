package search;

public class BinarySearch implements Search{

    public int findIndex(Comparable[] array, Comparable element){
        return this.findIndexElement(array, 0, array.length - 1, element);
    }

    private int findIndexElement(Comparable[] array, int startIndex, int endIndex, Comparable element){
        if(endIndex >= startIndex) {
            var middleIndex = startIndex + (endIndex - startIndex) / 2;
            if (array[middleIndex].compareTo(element) == 0)
                return middleIndex;
            if (array[middleIndex].compareTo(element) == -1)
                return findIndexElement(array, middleIndex + 1, endIndex, element);
            return findIndexElement(array, startIndex, middleIndex - 1, element);
        }
        return -1;
    }

}
