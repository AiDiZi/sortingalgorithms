package search;

public interface Search {

    int findIndex(Comparable[] array, Comparable element);

}
