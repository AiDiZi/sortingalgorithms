package sort;

public class MergeSort implements SortAlgorithm {

    public Comparable[] doSort(Comparable[] arr){
        if(arr.length == 1 || arr.length == 0)
            return arr;
        if(arr.length == 2){
            if(arr[0].compareTo(arr[1]) == -1){
                return new Comparable[] {arr[0], arr[1]};
            }
            else {
                return new Comparable[] {arr[1], arr[0]};
            }
        }
        var middle = (arr.length % 2 != 0) ? arr.length / 2 + 1 : arr.length / 2;
        var firstPart = getPartArr(arr,0, middle);
        var secondPart = getPartArr(arr, middle ,arr.length);
        return concatArrs(doSort(firstPart), doSort(secondPart));
    }

    private Comparable[] concatArrs(Comparable[] firstArr, Comparable[] secondArr){
        var outArr = new Comparable[firstArr.length + secondArr.length];
        var lenght = Math.min(firstArr.length, secondArr.length);
        int indexFirstArr = 0;
        int indexSecondArr = 0;
        int k = 0;

        while(indexFirstArr < lenght && indexSecondArr < lenght){
            if(firstArr[indexFirstArr].compareTo(secondArr[indexSecondArr]) == -1){
                outArr[k] = firstArr[indexFirstArr];
                indexFirstArr++;
            }
            else{
                outArr[k] = secondArr[indexSecondArr];
                indexSecondArr++;
            }
            k++;
        }
        if(indexFirstArr < firstArr.length && indexSecondArr < secondArr.length){
            if(firstArr[indexFirstArr].compareTo(secondArr[indexSecondArr]) == 1){
                while(indexSecondArr < secondArr.length){
                    outArr[k] = secondArr[indexSecondArr];
                    k++;
                    indexSecondArr++;
                }
                while(indexFirstArr < firstArr.length){
                    outArr[k] = firstArr[indexFirstArr];
                    k++;
                    indexFirstArr++;
                }
            }
        }
        while(indexFirstArr < firstArr.length){
            outArr[k] = firstArr[indexFirstArr];
            k++;
            indexFirstArr++;
        }
        while(indexSecondArr < secondArr.length){
            outArr[k] = secondArr[indexSecondArr];
            k++;
            indexSecondArr++;
        }
        return outArr;
    }

}
