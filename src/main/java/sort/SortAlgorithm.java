package sort;

public interface SortAlgorithm {

    Comparable[] doSort(Comparable[] arr);

    default Comparable[] getPartArr(Comparable[] arr, int startIndex, int lastIndex) {
        var outArr = new Comparable[lastIndex - startIndex];
        for(int i = startIndex; i < lastIndex ; i++){
            outArr[i - startIndex] = arr[i];
        }
        return outArr;
    }

}
