package sort;

public class QuickSort implements SortAlgorithm{

    public Comparable[] doSort(Comparable[] inputArr){
        if(inputArr.length == 1 || inputArr.length == 0)
            return inputArr;
        var nowMiddle = inputArr.length / 2;
        var leftPartIndex = 0;
        var rightPartIndex = inputArr.length - 1;
        while(leftPartIndex < nowMiddle || rightPartIndex > nowMiddle){
            if((inputArr[leftPartIndex].compareTo(inputArr[nowMiddle]) == 1 || inputArr[leftPartIndex].compareTo(inputArr[nowMiddle]) == 0)&&
                    (inputArr[rightPartIndex].compareTo(inputArr[nowMiddle]) == 0 ||inputArr[rightPartIndex].compareTo(inputArr[nowMiddle]) == -1)){
                var changer = inputArr[leftPartIndex];
                inputArr[leftPartIndex] = inputArr[rightPartIndex];
                inputArr[rightPartIndex] = changer;
                leftPartIndex++;
                rightPartIndex--;
            }
            else{
                if(inputArr[leftPartIndex].compareTo(inputArr[nowMiddle]) == -1
                    || inputArr[leftPartIndex].compareTo(inputArr[nowMiddle]) == 0)
                    leftPartIndex++;
                if(inputArr[rightPartIndex].compareTo(inputArr[nowMiddle]) == 1
                    || inputArr[rightPartIndex].compareTo(inputArr[nowMiddle]) == 0)
                    rightPartIndex--;
            }
        }
        var middle = (inputArr.length % 2 != 0) ? inputArr.length / 2 + 1 : inputArr.length / 2;
        var firstPart = getPartArr(inputArr,0, middle);
        var secondPart = getPartArr(inputArr, middle ,inputArr.length);
        return concatArrs(doSort(firstPart), doSort(secondPart));
    }

    private Comparable[] concatArrs(Comparable[] firstArr, Comparable[] secondArr){
        var outArr = new Comparable[firstArr.length + secondArr.length];
        for(int i = 0; i < firstArr.length; i++)
            outArr[i] = firstArr[i];
        for(int i = 0; i < secondArr.length; i++)
            outArr[i + firstArr.length] = secondArr[i];
        return outArr;
    }

}
